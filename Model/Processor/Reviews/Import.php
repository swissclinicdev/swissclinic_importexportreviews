<?php

namespace Swissclinic\ImportExportReviews\Model\Processor\Reviews;

use Magento\Framework\File\Csv;
use Magento\Review\Model\Review;
use Magento\Review\Model\Rating;
use Magento\Framework\Json\Decoder;
use Magento\Framework\Message\ManagerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;

class Import
{
    /**
     * Review Columns
     */
    const COL_NICKNAME          = 'nickname';
    const COL_CREATED_AT        = 'created_at';
    const COL_ENTITY_PK_VALUE   = 'entity_pk_value';
    const COL_STATUS_ID         = 'status_id';
    const COL_TITLE             = 'title';
    const COL_DETAIL            = 'detail';
    const COL_CUSTOMER_ID       = 'customer_id';
    const COL_STORE             = 'store_id';
    const COL_ENTITY_ID         = 'entity_id';
    const COL_STORES            = 'stores';

    /**
     * Rating Columns
     */
    const COL_REVIEW_ID = 'review_id';
    const COL_OPTION_VALUE = '';

    /**
     * Validation Messages
     */

    const ERROR_EMPTY_SKU = 'sku field is empty.';
    const ERROR_EMPTY_NICKNAME = 'nickname field is empty.';
    const ERROR_EMPTY_TITLE = 'title field is empty.';
    const ERROR_EMPTY_DETAIL = 'detail field is empty.';
    const ERROR_EMPTY_STATUS = 'status field is empty.';
    const ERROR_EMPTY_STORE = 'store field is empty.';

    /**
     * @var Csv $_csv
     */
    protected $_csv;

    /**
     * @var Review $_review
     */
    protected $_review;

    /**
     * @var Rating $_rating
     */
    protected $_rating;

    /**
     * @var Decoder $_jsonDecoder
     */
    protected $_jsonDecoder;

    /**
     * @var ManagerInterface $_messageManager
     */
    protected $_messageManager;

    /**
     * @var ProductRepositoryInterface $_productRepository
     */
    protected $_productRepository;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var StoreRepositoryInterface
     */
    protected $_storeRepository;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var array $_mappings
     */
    protected $_validationMappings = [
        'sku' => [
            'required' => true,
            'error' => self::ERROR_EMPTY_SKU
        ],
        'nickname' => [
            'required' => true,
            'error' => self::ERROR_EMPTY_NICKNAME
        ],
        'title' => [
            'required' => true,
            'error' => self::ERROR_EMPTY_TITLE
        ],
        'detail' => [
            'required' => true,
            'error' => self::ERROR_EMPTY_DETAIL
        ],
        'status' => [
            'required' => true,
            'error' => self::ERROR_EMPTY_STATUS
        ],
        'store' => [
            'required' => true,
            'error' => self::ERROR_EMPTY_STORE
        ],
    ];

    /**
     * @var array $_reviewMapping
     */
    protected $_reviewMapping = [
        'product_id'    => self::COL_ENTITY_PK_VALUE,
        'nickname'      => self::COL_NICKNAME,
        'title'         => self::COL_TITLE,
        'detail'        => self::COL_DETAIL,
        'status'        => self::COL_STATUS_ID,
        'created_at'    => self::COL_CREATED_AT,
        'customer_id'   => self::COL_CUSTOMER_ID,
        'store'         => self::COL_STORE,
        'entity_id'     => self::COL_ENTITY_ID,
        'stores'        => self::COL_STORES
    ];

    /**
     * @var array $_ratingMaping
     */
    protected $_ratingMaping = [
        'review' => self::COL_REVIEW_ID,
    ];

    /**
     * @var array $_ratingOptionMapping
     */
    protected $_ratingOptionMapping = [
        1 => [
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5
        ],
        2 => [
            1 => 6,
            2 => 7,
            3 => 8,
            4 => 9,
            5 => 10
        ],
        3 => [
            1 => 11,
            2 => 12,
            3 => 13,
            4 => 14,
            5 => 15
        ],
    ];

    /**
     * @var array $_ratingTitleMapping
     */
    protected $_ratingTitleMapping = [
        'Quality' => 1,
        'Value' => 2,
        'Price' => 3
    ];

    protected $_statusMapping = [
        'Approved' => 1,
        'Pending' => 2,
        'Not Approved' => 3
    ];

    /**
     * @var array|null $_importIndexes
     */
    protected $_importIndexes;

    /**
     * @var array $_rowsData
     */
    protected $_rowsData = [];

    /**
     * @var mixed $_rowFlag
     */
    protected $_rowFlag;

    /**
     * Review Entity Product
     *
     * @var string $_entity_id
     */
    protected $_entity_id = '1';

    /**
     * Import constructor.
     * @param Csv $csv
     * @param Review $review
     * @param Rating $rating
     * @param ManagerInterface $messageManager
     * @param ProductRepositoryInterface $productRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param StoreRepositoryInterface $storeRepository
     * @param DateTime $dateTime
     */
    public function __construct(
        Csv $csv,
        Review $review,
        Rating $rating,
        ManagerInterface $messageManager,
        ProductRepositoryInterface $productRepository,
        CustomerRepositoryInterface $customerRepository,
        StoreRepositoryInterface $storeRepository,
        DateTime $dateTime
    )
    {
        $this->_csv = $csv;
        $this->_review = $review;
        $this->_rating = $rating;
        $this->_messageManager = $messageManager;
        $this->_productRepository = $productRepository;
        $this->_customerRepository = $customerRepository;
        $this->_storeRepository = $storeRepository;
        $this->_dateTime = $dateTime;

    }

    /**
     * @param $file
     * @throws \Exception
     */
    protected function _parseCsv($file)
    {
        $this->_rowsData = $this->_csv->getData($file);
        if (empty($this->_importIndexes)) {
            $this->_importIndexes = $this->_rowsData[0];
        }
        unset($this->_rowsData[0]);
    }

    /**
     * Validate Rows
     *
     * @param array $rowData
     * @return bool
     */
    protected function _validate($rowData = [])
    {
        $isValid = true;
        foreach ($rowData as $index => $value) {
            if (isset($this->_validationMappings[$index]['required']) && !isset($value)) {
                $this->_messageManager->addError(
                    __('Error on row %1, %2', $this->_rowFlag, $this->_validationMappings[$index]['error'])
                );

                $isValid = false;
            }

            if ($index === 'customer_id' && !empty($value)) {
                $isValid = $this->_getCustomerById($value);
            }

            if ($index === 'sku' && !empty($value)) {
                $isValid = $this->_getProductId($value);
            }
        }

        return $isValid;
    }

    /**
     * Process Import
     *
     * @param $file
     * @throws \Exception
     */
    public function import($file)
    {
        $this->_parseCsv($file);
        $this->_prepareIndex();

        $rows = 0;

        foreach ($this->_rowsData as $rowNum => $data) {
            $this->_rowFlag = $rowNum;
            if (!$this->_validate($data) || !$data['product_id']) {
                continue;
            }

            $reviewData = [];
            foreach ($this->_reviewMapping as $fileIndex => $reviewIndex) {
                $reviewData[$reviewIndex] = isset($data[$fileIndex]) ? $data[$fileIndex] : null;
            }

            $reviewId = $this->_saveReview($reviewData);
            $ratingData = $this->_parseRatingDetails($data['rating_details']);

            $this->_saveRating($ratingData, $reviewId, $data['product_id']);

            $rows++;
        }

        $this->_messageManager->addSuccess(__("Successfully imported %1 rows", $rows));
    }

    /**
     * @param $customer_id
     * @return bool
     */
    protected function _getCustomerById($customer_id)
    {
        try {
            $this->_customerRepository->getById($customer_id);
            return true;
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __('Error on row %1. %2', $this->_rowFlag, $e->getMessage())
            );
        }

        return false;
    }

    /**
     * @param string $code
     * @return bool|int
     */
    protected function _getStoreByCode($code = '')
    {
        try {
            $storeRepository = $this->_storeRepository->get($code);
            return $storeRepository->getId();
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __('Error on row %1. %2', $this->_rowFlag, $e->getMessage())
            );
        }

        return false;
    }

    /**
     * @param string $sku
     * @return int|null|bool
     */
    protected function _getProductId($sku = '')
    {
        try {
            $product = $this->_productRepository->get($sku);
            return $product->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $this->_messageManager->addError(
                __('Error on row %1. %2', $this->_rowFlag, $e->getMessage())
            );
        }

        return false;
    }

    /**
     * @param $ratingData
     * @return array
     */
    protected function _parseRatingDetails($ratingData)
    {
        $ratingDetails = json_decode($ratingData, true);
        $result = [];
        foreach ($ratingDetails as $index => $data) {
            $rating = $this->_ratingTitleMapping[$data['rating']];
            $value = $this->_ratingOptionMapping[$rating][$data['value']];
            $result[$rating] = $value;
        }

        return $result;
    }

    /**
     * @param array $reviewData
     * @return mixed
     * @throws \Exception
     */
    protected function _saveReview($reviewData)
    {
        try {
            $reviewModel = $this->_review;
            $reviewData = array_filter($reviewData);
            foreach ($reviewData as $column => $value) {
                $reviewModel->setData($column, $value);
            }
            $reviewModel->save();
            $lastId = $reviewModel->getId();
            $reviewModel->aggregate();
            /** Re-save created_at to apply changes */
            $reviewModel->setCreatedAt($reviewData['created_at']);
            $reviewModel->save();
            $reviewModel->unsetData();

            return $lastId;
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __('Error on row %1. %2', $this->_rowFlag, $e->getMessage())
            );
        }

        return false;
    }

    /**
     * @param array $data
     * @param $review_id
     * @param $product_id
     * @return bool
     */
    protected function _saveRating($data, $review_id, $product_id)
    {
        if (empty($review_id) || empty($product_id)) {
            return false;
        }

        try {
            foreach ($data as $ratingId => $value) {
                $ratingModel = $this->_rating;
                $ratingModel->setRatingId($ratingId)
                    ->setReviewId($review_id)
                    ->addOptionVote($value, $product_id);
            }

            return true;
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __('Error on row %1. %2', $this->_rowFlag, $e->getMessage())
            );
        }

        return false;
    }

    /**
     * Set Values keys
     */
    protected function _prepareIndex()
    {
        $rowData = $this->_rowsData;
        foreach ($rowData as $index => $data) {
            $this->_rowsData[$index] = array_combine($this->_importIndexes, $data);

            if (in_array($this->_rowsData[$index]['status'], array_keys($this->_statusMapping))) {
                $this->_rowsData[$index]['status'] = $this->_statusMapping[$this->_rowsData[$index]['status']];
            }

            /** created_at should be DD-MM-YYYY H:i:s */
            $importRowDate = (string) $this->_rowsData[$index]['created_at'];
            $this->_rowsData[$index]['created_at'] = date('Y-m-d H:i:s', strtotime($importRowDate));

            if (isset($this->_rowsData[$index]['store'])) {
                $this->_rowsData[$index]['store'] = $this->_getStoreByCode($this->_rowsData[$index]['store']);
            }

            if (isset($this->_rowsData[$index]['sku'])) {
                $this->_rowsData[$index]['product_id'] = $this->_getProductId($this->_rowsData[$index]['sku']);
            }

            $this->_rowsData[$index]['stores'] = $this->_getStoreByCode($this->_rowsData[$index]['store']);
            $this->_rowsData[$index]['entity_id'] = $this->_entity_id;
        }
    }

    protected function _log($message = '')
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/rj_debug.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }
}