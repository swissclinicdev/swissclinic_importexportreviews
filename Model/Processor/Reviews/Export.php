<?php

namespace Swissclinic\ImportExportReviews\Model\Processor\Reviews;

use Magento\Review\Model\ResourceModel\Review\CollectionFactory as ReviewCollection;
use Magento\Review\Model\Rating\Option\VoteFactory as VoteCollection;
use Magento\Customer\Model\Customer;
use Magento\Catalog\Model\Product;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Message\ManagerInterface;

class Export
{
    /**
     * @var ReviewCollection $_reviewCollection
     */
    protected $_reviewCollection;

    /**
     * @var VoteCollection $_voteCollection
     */
    protected $_voteCollection;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var Customer $_customer
     */
    protected $_customer;

    /**
     * @var Product $_product
     */
    protected $_product;

    /**
     * @var StoreManagerInterface $_storeManager
     */
    protected $_storeManager;

    /**
     * @var array
     */
    protected $_rowHeaders;

    /**
     * @var array
     */
    protected $_rowData = [];

    /**
     * @var array
     */
    protected $_reviewColumns = [
        'review_id',
        'nickname',
        'created_at',
        'title',
        'detail',
        'status_id',
    ];

    /**
     * @var array
     */
    protected $_statusMapping = [
        '1' => 'Approved',
        '2' => 'Pending',
        '3' => 'Not Approved'
    ];

    /**
     * @var array
     */
    protected $_voteColumns = [
        'rating_id' => 'rating',
        'percent'   => 'percentage',
        'value'     => 'value'
    ];

    /**
     * @var array
     */
    protected $_ratingName = [
        '1' => 'Quality',
        '2' => 'Value',
        '3' => 'Price'
    ];

    /**
     * @var array
     */
    protected $_customerColumns = [
        'entity_id',
        'firstname',
        'lastname'
    ];

    /**
     * @var array
     */
    protected $_exportColumns = [
        'review_id',
        'nickname',
        'created_at',
        'title',
        'detail',
        'status',
        'rating_details',
        'sku',
        'customer_id',
        'customer_firstname',
        'customer_lastname',
        'store'
    ];

    /**
     * Export constructor.
     * @param ReviewCollection $reviewCollection
     * @param VoteCollection $voteCollection
     * @param Customer $customer
     * @param Product $product
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ReviewCollection $reviewCollection,
        VoteCollection $voteCollection,
        Customer $customer,
        Product $product,
        StoreManagerInterface $storeManager,
        ManagerInterface $messageManager
    )
    {
        $this->_reviewCollection = $reviewCollection->create()->addStoreData()->load();
        $this->_voteCollection   = $voteCollection;
        $this->_customer         = $customer;
        $this->_product          = $product;
        $this->_storeManager     = $storeManager;
        $this->_messageManager   = $messageManager;
    }

    /**
     * Generate Data for export.
     *
     * @return array
     */
    public function generate()
    {
        $this->_addHeaders();
        $this->_buildData();
        return $this->_rowData;
    }

    /**
     * Build Rows
     */
    protected function _buildData()
    {
        try {
            foreach ($this->_reviewCollection as $item => $review) {
                $reviews = $this->_addReviewData($review);
                $vote = $this->_addVoteData($review);
                $product = $this->_addProductData($review);
                $customer = $this->_addCustomerData($review);
                $store = $this->_addStoreData($review);

                $this->_rowData[] = array_merge($reviews, $vote, $product, $customer, $store);
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __("Unable to export." . $e->getMessage())
            );
        }
    }

    /**
     * Add CSV file headers
     */
    protected function _addHeaders()
    {
        if (empty($this->_rowHeaders)) {
            $this->_rowHeaders = $this->_exportColumns;
        }

        $this->_rowData[] = $this->_rowHeaders;
    }

    /**
     * @param $review
     * @return array
     */
    protected function _addReviewData($review)
    {
        $review_data = [];
        foreach ($this->_reviewColumns as $index) {
            $review_data[] = $this->_mapper($index, $review->getData($index));
        }

        return $review_data;
    }

    /**
     * @param $index
     * @param $value
     * @return mixed
     */
    protected function _mapper($index, $value)
    {
        if ($index === 'status_id') {
            $value = $this->_statusMapping[$value];
        } elseif ($index === 'rating_id') {
            $value = $this->_ratingName[$value];
        }

        return $value;
    }

    /**
     * @param $review
     * @return array
     */
    protected function _addVoteData($review)
    {
        $votesCollection = $this->_voteCollection->create()->getResourceCollection()
            ->setReviewFilter($review->getId())
            ->addOptionInfo()
            ->addRatingOptions()
            ->load();

        $ratingData = [];
        foreach ($votesCollection as $vote => $voteData) {
            $ratingValues = [];
            foreach ($this->_voteColumns as $index => $label) {
                $ratingValues[$label] = $this->_mapper($index, $voteData[$index]);
            }
            $ratingData[] = $ratingValues;
        }

        $result = [json_encode($ratingData)];
        return $result;
    }

    /**
     * @param $review
     * @return array
     */
    protected function _addProductData($review)
    {
        $this->_product->load($review->getData('entity_pk_value'));
        $productData = [$this->_product->getData('sku')];

        return $productData;
    }

    /**
     * @param $review
     * @return array
     */
    protected function _addCustomerData($review)
    {
        $customerData = [];
        $this->_customer->load($review->getData('customer_id'));
        foreach ($this->_customerColumns as $index) {
            $customerData[] = $this->_customer->getData($index);
        }
        $this->_customer->unsetData();

        return $customerData;
    }

    /**
     * @param $review
     * @return array
     */
    protected function _addStoreData($review)
    {
        $storeData = [];
        $stores = $review->getData('stores');
        $reviewStore = isset($stores[1]) ? $stores[1] : 0;
        foreach ($this->_storeManager->getStores(true, false) as $store) {
            if ($store->getId() === $reviewStore) {
                $storeData[] = $store->getCode();
            }
        }

        return $storeData;
    }
}