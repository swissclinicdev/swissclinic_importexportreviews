<?php

namespace Swissclinic\ImportExportReviews\Block\Adminhtml\Reviews\Import\Edit;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;

class Form extends Generic
{
    /**
     * @var FormFactory $_formFactory
     */
    protected $_formFactory;

    /**
     * Form constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     */
    public function __construct(Context $context, Registry $registry, FormFactory $formFactory)
    {
        $this->_formFactory = $formFactory;
        parent::__construct($context, $registry, $formFactory);
    }

    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'review_import_edit_form',
                    'action' => $this->getUrl('adminhtml/*/import'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ]
            ]
        );

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Import')]);

        $fieldset->addField(
            'import_file',
            'file',
            [
                'name' => 'import_file',
                'label' => __('Import File'),
                'title' => __('Import File'),
                'required' => false,
                'after_element_html' => '<button type="submit" style="margin-left: 20px;">Import!</button>'
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}