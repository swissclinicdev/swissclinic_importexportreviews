<?php

namespace Swissclinic\ImportExportReviews\Block\Adminhtml\Reviews\Export\Edit;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\ImportExport\Model\Source\Export\FormatFactory;

class Form extends Generic
{
    /**
     * @var FormatFactory $_formatFactory
     */
    protected $_formatFactory;

    /**
     * Form constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param FormatFactory $formatFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        FormatFactory $formatFactory
    )
    {

        $this->_formatFactory = $formatFactory;
        parent::__construct($context, $registry, $formFactory);
    }

    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'review_export_edit_form',
                    'action' => $this->getUrl('adminhtml/*/export'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ],
            ]
        );

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Export')]);

        $fieldset->addField(
            'file_format',
            'select',
            [
                'name' => 'file_format',
                'title' => __('Export File Format'),
                'label' => __('Export File Format'),
                'required' => false,
                'values' => $this->_formatFactory->create()->toOptionArray(),
                'value' => 'csv',
                'style' => 'width: 270px',
                'after_element_html' => '<button type="submit" style="margin-left: 20px;">Export!</button>'
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
