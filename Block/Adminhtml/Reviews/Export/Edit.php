<?php

namespace Swissclinic\ImportExportReviews\Block\Adminhtml\Reviews\Export;

use Magento\Backend\Block\Widget\Form\Container;

class Edit extends Container
{
    /**
     *  Container Constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->removeButton('back')
            ->removeButton('reset')
            ->removeButton('save');

        $this->_objectId = 'importexportreviews_id';
        $this->_blockGroup = 'Swissclinic_ImportExportReviews';
        $this->_controller = 'adminhtml_reviews_export';
    }

}
