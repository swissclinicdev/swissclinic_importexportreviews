<?php

namespace Swissclinic\ImportExportReviews\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const EXPORT_ENTITY = 'review';
}