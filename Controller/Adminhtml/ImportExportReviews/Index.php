<?php

namespace Swissclinic\ImportExportReviews\Controller\Adminhtml\ImportExportReviews;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{

    protected $_request;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\Http $request)
    {
        parent::__construct($context);

        $this->_request = $request;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Swissclinic_ImportExportReviews::reviews_importexport');
        $resultPage->getConfig()->getTitle()->prepend(__('Reviews Import/Export'));
        return $resultPage;
    }
}