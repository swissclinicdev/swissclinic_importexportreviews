<?php

namespace Swissclinic\ImportExportReviews\Controller\Adminhtml\ImportExportReviews;

use Magento\Backend\App\Action;
use Magento\Framework\File\Csv;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Swissclinic\ImportExportReviews\Model\Processor\Reviews\Export as ReviewsGenerator;

class Export extends Action
{
    /**
     * @var ReviewsGenerator $_reviewsGenerator
     */
    protected $_reviewsGenerator;

    /**
     * @var DirectoryList $_directoryList
     */
    protected $_directoryList;

    /**
     * @var FileFactory $_fileFactory
     */
    protected $_fileFactory;

    /**
     * @var Csv $_csvAdapter
     */
    protected $_csvAdapter;

    /**
     * Export constructor.
     * @param Action\Context $context
     * @param DirectoryList $directoryList
     * @param Csv $csvAdapter
     * @param FileFactory $fileFactory
     * @param ReviewsGenerator $reviewsGenerator
     */
    public function __construct(
        Action\Context $context,
        DirectoryList $directoryList,
        Csv $csvAdapter,
        FileFactory $fileFactory,
        ReviewsGenerator $reviewsGenerator
    )
    {
        parent::__construct($context);
        $this->_directoryList    = $directoryList;
        $this->_csvAdapter       = $csvAdapter;
        $this->_fileFactory      = $fileFactory;
        $this->_reviewsGenerator = $reviewsGenerator;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $post = $this->getRequest()->getParams();
        if (!empty($post)) {
            try {
                $rowsData = $this->_reviewsGenerator->generate();
                $filepath = $this->_filepath() . '/' . $this->_filename();
                $this->_csvAdapter->saveData($filepath, $rowsData);

                /** Download File */
                $this->download();
            } catch (\Exception $e) {
                $this->messageManager->addError(__($e->getMessage()));
            }
        }
    }

    /**
     * @return string
     */
    protected function _filename()
    {
        return 'reviews_' . date('Ymd_His') . '.csv';
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function _filepath()
    {
        return $this->_directoryList->getPath(DirectoryList::ROOT);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Exception
     */
    public function download()
    {
        return $this->_fileFactory->create(
            $this->_filename(),
            [
                'type' => 'filename',
                'value' => $this->_filename(),
                'rm' => true
            ]
        );
    }
}