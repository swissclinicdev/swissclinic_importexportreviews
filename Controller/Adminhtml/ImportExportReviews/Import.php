<?php

namespace Swissclinic\ImportExportReviews\Controller\Adminhtml\ImportExportReviews;

use Magento\Backend\App\Action;
use Magento\Framework\Filesystem;
use Magento\ImportExport\Helper\Data as ImportExportHelper;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\HTTP\Adapter\FileTransferFactory;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Swissclinic\ImportExportReviews\Model\Processor\Reviews\Import as ImportProcessor;

class Import extends Action
{
    /**
     * @var ImportProcessor $_importProcessor
     */
    protected $_importProcessor;

    /**
     * @var FileTransferFactory $_httpFactory
     */
    protected $_httpFactory;

    /**
     * @var UploaderFactory $_uploaderFactory
     */
    protected $_uploaderFactory;

    /**
     * @var Filesystem\Directory\WriteInterface $_varDirectory
     */
    protected $_varDirectory;

    /**
     * @var ImportExportHelper $_importExportHelper
     */
    protected $_importExportHelper;

    /**
     * Import source file.
     */
    const FIELD_NAME_SOURCE_FILE = 'import_file';

    /**
     * Import constructor.
     * @param Action\Context $context
     * @param ImportProcessor $importProcessor
     * @param FileTransferFactory $httpFactory
     * @param UploaderFactory $uploaderFactory
     * @param Filesystem $filesystem
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Action\Context $context,
        ImportProcessor $importProcessor,
        FileTransferFactory $httpFactory,
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem,
        ImportExportHelper $importExportHelper
    )
    {
        $this->_importProcessor = $importProcessor;
        $this->_httpFactory = $httpFactory;
        $this->_uploaderFactory = $uploaderFactory;
        $this->_varDirectory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->_importExportHelper = $importExportHelper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $file_data = $this->_fileUpload();
            $this->_importProcessor->import($file_data);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError(__($e->getMessage()));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('adminhtml/*/index');
        return $resultRedirect;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _fileUpload()
    {
        $adapter = $this->_httpFactory->create();
        if (!$adapter->isValid(self::FIELD_NAME_SOURCE_FILE)) {
            $errors = $adapter->getErrors();
            if ($errors[0] == \Zend_Validate_File_Upload::INI_SIZE) {
                $errorMessage = $this->_importExportHelper->getMaxUploadSizeMessage();
            } else {
                $errorMessage = __('The file was not uploaded.');
            }
            throw new \Magento\Framework\Exception\LocalizedException($errorMessage);
        }

        /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
        $uploader = $this->_uploaderFactory->create(['fileId' => self::FIELD_NAME_SOURCE_FILE]);
        $uploader->skipDbProcessing(true);
        $uploader->setAllowedExtensions(['csv']);
        $uploader->setAllowRenameFiles(true);
        $result = $uploader->save($this->_getImportExportDir());

        return $this->_getFilePath($result);
    }

    /**
     * @param $file_upload
     * @return string
     */
    protected function _getFilePath($file_upload)
    {
        return $file_upload['path'] . $file_upload['file'];
    }

    /**
     * @return string
     */
    protected function _getImportExportDir()
    {
        return $this->_varDirectory->getAbsolutePath('importexport/');
    }
}
